import {OperatorExpression} from './OperatorExpression';

export class Add extends OperatorExpression {
    static PRECEDENCE = 1;

    evaluateOperator(left: number, right: number): number {
        return left + right;
    }

    operatorSign() {
        return '+';
    }

    getPrecedence() {
        return Add.PRECEDENCE;
    }
}
