export interface IMathExpression {
    evaluate(): number;
    toString(): string;
    render(precedenceOfParent?: number): string;
}
