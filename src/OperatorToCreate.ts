import {Constant} from './Constant';
import {IBuilder} from './IBuilder';
import {IMathExpression} from './IMathExpression';
import {Operator} from './CalculatorMathExpressionBuilder';

export class OperatorToCreate implements IBuilder<IMathExpression> {
    precedence: number;

    constructor(private operator: Operator,
                private right: IMathExpression,
                private prev: OperatorToCreate | null,
                private currentExpression?: IMathExpression) {
        this.precedence = operator.PRECEDENCE;
    }

    build(): IMathExpression {
        let left: IMathExpression;
        if (this.prev !== null) {
            left = this.prev.right;
            const operatorExpression = new this.operator(left, this.right);
            this.prev.right = operatorExpression;

            return this.prev.build();
        } else if (this.currentExpression) {
            left = this.currentExpression;
            return new this.operator(left, this.right);
        }
        return new Constant(1);
    }
}
