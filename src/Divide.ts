import {nullable} from './Nullable';
import {OperatorExpression} from './OperatorExpression';

function log(target: object, name: string, descriptor: PropertyDescriptor) {
    console.log('Log: ', name);
}

export class Divide extends OperatorExpression {
    static PRECEDENCE = 2;

    @log
    @nullable([false, true])
    evaluateOperator(left: number, right: number): number {
        return left / right;
    }
    @log
    operatorSign() {
        return '/';
    }

    getPrecedence() {
        return Divide.PRECEDENCE;
    }
}
