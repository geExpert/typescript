import {Add} from './Add';
import {Constant} from './Constant';
import {Divide} from './Divide';
import {IBuilder} from './IBuilder';
import {IMathExpression} from './IMathExpression';
import {Multiply} from './Multiply';
import {Subtract} from './Substract';

export class MathExpressionBuilder implements IBuilder<IMathExpression> {
    static startWith(num: number): MathExpressionBuilder {
        return new MathExpressionBuilder(num);
    }

    currentExpression: IMathExpression;

    constructor(num: number) {
        this.currentExpression = new Constant(num);
    }

    add(num: number): MathExpressionBuilder {
        this.currentExpression = new Add(this.currentExpression, new Constant(num));
        return this;
    }

    subtract(num: number): MathExpressionBuilder {
        this.currentExpression = new Subtract(this.currentExpression, new Constant(num));
        return this;
    }

    multiplyBy(num: number): MathExpressionBuilder {
        this.currentExpression = new Multiply(this.currentExpression, new Constant(num));
        return this;
    }

    divideBy(num: number): MathExpressionBuilder {
        this.currentExpression = new Divide(this.currentExpression, new Constant(num));
        return this;
    }

    build() {
        return this.currentExpression;
    }
}
