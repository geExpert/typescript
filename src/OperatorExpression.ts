import {IMathExpression} from './IMathExpression';

export abstract class OperatorExpression implements IMathExpression {
    static PRECEDENCE: number;
    constructor(protected leftExpression: IMathExpression, protected rightExpression: IMathExpression) {

    }

    evaluate(): number {
        const left = this.leftExpression.evaluate();
        const right = this.rightExpression.evaluate();

        return this.evaluateOperator(left, right);
    }

    toString() {
        return this.render(Number.MIN_SAFE_INTEGER);
    }

    render(precedenceOfParent: number) {
        const myPrecedence = this.getPrecedence();

        const leftRendered = this.leftExpression.render(myPrecedence);
        const rightRendered = this.rightExpression.render(myPrecedence);

        let rendered = `${leftRendered} ${this.operatorSign()} ${rightRendered}`;

        if (precedenceOfParent > myPrecedence) {
            rendered = `(${rendered})`;
        }

        return rendered;
    }

    protected abstract evaluateOperator(left: number, right: number): number;

    protected abstract operatorSign(): string;

    protected abstract getPrecedence(): number;
}
