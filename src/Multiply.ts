import {OperatorExpression} from './OperatorExpression';

export class Multiply extends OperatorExpression {
    static PRECEDENCE = 2;
    evaluateOperator(left: number, right: number): number {
        return left * right;
    }

    operatorSign() {
        return '*';
    }

    getPrecedence() {
        return Multiply.PRECEDENCE;
    }
}
