import {IMathExpression} from './IMathExpression';

export class Constant implements IMathExpression {
    constructor(private value: number) {
    }

    evaluate() {
        return this.value;
    }

    toString() {
        return this.value.toString();
    }

    render() {
        return this.toString();
    }
}
