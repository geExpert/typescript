import {OperatorExpression} from './OperatorExpression';

export class Subtract extends OperatorExpression {
    static PRECEDENCE = 1;
    evaluateOperator(left: number, right: number): number {
        return left - right;
    }

    operatorSign() {
        return '-';
    }

    getPrecedence() {
        return Subtract.PRECEDENCE;
    }
}
