import {Add} from './Add';
import {Constant} from './Constant';
import {Divide} from './Divide';
import {IBuilder} from './IBuilder';
import {IMathExpression} from './IMathExpression';
import {Multiply} from './Multiply';
import {OperatorToCreate} from './OperatorToCreate';
import {Subtract} from './Substract';

export type Operator = typeof Add | typeof Subtract | typeof Multiply | typeof Divide;

export class CalculatorMathExpressionBuilder implements IBuilder<IMathExpression> {
    static startWith(num: number): CalculatorMathExpressionBuilder {
        return new CalculatorMathExpressionBuilder(num);
    }
    currentExpression: IMathExpression;

    operatorToCreate: OperatorToCreate | null = null;

    constructor(num: number) {
        this.currentExpression = new Constant(num);
    }

    newOperator(operator: Operator, valueExpression: IMathExpression) {
        if (this.operatorToCreate != null && this.operatorToCreate.precedence >= operator.PRECEDENCE) {
            this.currentExpression = this.operatorToCreate.build();
            this.operatorToCreate = null;
        }
        this.operatorToCreate = new OperatorToCreate(
            operator,
            valueExpression,
            this.operatorToCreate,
            this.currentExpression);
    }

    add(num: number): CalculatorMathExpressionBuilder {
        this.newOperator(Add, new Constant(num));
        return this;
    }

    subtract(num: number): CalculatorMathExpressionBuilder {
        this.newOperator(Subtract, new Constant(num));
        return this;
    }

    multiplyBy(num: number): CalculatorMathExpressionBuilder {
        this.newOperator(Multiply, new Constant(num));
        return this;
    }

    divideBy(num: number): CalculatorMathExpressionBuilder {
        this.newOperator(Divide, new Constant(num));
        return this;
    }

    build() {
        if (this.operatorToCreate !== null) {
            this.currentExpression = this.operatorToCreate.build();
            this.operatorToCreate = null;
        }

        return this.currentExpression;
    }
}
