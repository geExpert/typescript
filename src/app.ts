import {Add} from './Add';
import {CalculatorMathExpressionBuilder} from './CalculatorMathExpressionBuilder';
import {Constant} from './Constant';
import {Divide} from './Divide';
import {MathExpressionBuilder} from './MathExpressionBuilder';
import {Multiply} from './Multiply';
import {Subtract} from './Substract';

const exp = new Divide(new Constant(123), new Constant(0));
try {
    console.log(`${exp.toString()} = ${exp.evaluate()}`);
} catch (e) {
    console.log(`${exp.toString()} => ${e.message}`);
}
const complexExpression = new Multiply(
    new Add(
        new Constant(123),
        new Add(
            new Constant(456),
            new Constant(789))),
    new Subtract(
        new Constant(3),
        new Divide(new Constant(16), new Constant(4)),
    ));

console.log(`${complexExpression.toString()} = ${complexExpression.evaluate()}`);

const expression1 = MathExpressionBuilder
    .startWith(1)
    .add(2)
    .add(3)
    .multiplyBy(4)
    .subtract(5)
    .divideBy(5).build();
console.log(`${expression1.toString()} = ${expression1.evaluate()}`);

const expression2 = CalculatorMathExpressionBuilder
    .startWith(1)
    .add(2)
    .add(3)
    .multiplyBy(4)
    .subtract(5)
    .divideBy(5).build();
console.log(`${expression2.toString()} = ${expression2.evaluate()}`);
