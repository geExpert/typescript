export function nullable(value: boolean[]) {
    const nullables = value;
    return (target: object, key: string, descriptor: any) => {
        return {
            value(...args: any[]) {
                args.forEach((val, index) => {
                    if (nullables[index] && val === 0) {
                        throw new Error('You can\'t divide with zero');
                    }
                });
                const result = descriptor.value.apply(this, args);
                return result;
            },
        };
    };
}
